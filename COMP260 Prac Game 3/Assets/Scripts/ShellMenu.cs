﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShellMenu : MonoBehaviour {

	public GameObject shellPanel;
	public GameObject optionsPanel;
	public Dropdown qualityDropdown;
	public Dropdown resolutionDropdown;
	public Toggle fullscreenToggle;
	public Slider volumeSlider;

	private bool paused = true;

	// Use this for initialization
	void Start () {
		optionsPanel.SetActive (false);
		SetPaused (paused);

		// quality drop down stuff
		qualityDropdown.ClearOptions();
		List<string> names = new List<string> ();
		for (int i = 0; i < QualitySettings.names.Length; i++) {
			names.Add (QualitySettings.names [i]);
		}
		qualityDropdown.AddOptions (names);

		// resolution settings stuff
		resolutionDropdown.ClearOptions();
		List<string> resolutions = new List<string> ();
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			resolutions.Add (Screen.resolutions [i].ToString ());
		}
		resolutionDropdown.AddOptions (resolutions);
	}
	
	// Update is called once per frame
	void Update () {
		if (!paused && Input.GetKeyDown(KeyCode.Escape)) {
				SetPaused(true);
			}
	}
	
	private void SetPaused(bool p) {
		paused = p;
		shellPanel.SetActive (paused);
		Time.timeScale = paused ? 0 : 1;
	}

	public void OnPressedPlay() {
		SetPaused (false);
	}

	public void onPressedOptions() {
		shellPanel.SetActive (false);
		optionsPanel.SetActive (true);

		// select current quality level
		qualityDropdown.value = QualitySettings.GetQualityLevel();

		// select current resolution
		int currentResolution = 0;
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			if (Screen.resolutions [i].width == Screen.width && Screen.resolutions [i].height == Screen.height) {
				currentResolution = i;
				break;
			}
		}
		resolutionDropdown.value = currentResolution;
	}

	public void OnPressedQuit() {
		Application.Quit();
	}

	public void OnPressedApply () {
		// apply changes
		QualitySettings.SetQualityLevel(qualityDropdown.value);
		Resolution res = Screen.resolutions [resolutionDropdown.value];
		Screen.SetResolution (res.width, res.height, true);

		// swap panels
		shellPanel.SetActive (true);
		optionsPanel.SetActive (false);
	}

	public void OnPressedCancel () {
		shellPanel.SetActive (true);
		optionsPanel.SetActive (false);
	}
}
