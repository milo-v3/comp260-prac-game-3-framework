﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {

	private Animator animator;
	public float startTime = 0.0f;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}

	void Destroy () {
		Destroy (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		animator.SetBool ("Start", Time.time >= startTime);
	}

	void OnCollisionEnter(Collision collision) {
		animator.SetBool ("Hit", true);
		Debug.Log ("Hit");
	}
}
