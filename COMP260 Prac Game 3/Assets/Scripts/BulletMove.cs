﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

	public float speed = 100.0f;
	public float decayTime = 2.0f;

	public Vector3 direction;
	private float startTime;

	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.velocity = speed * direction;
		if (Time.time >= (decayTime + startTime)) {
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter(Collision collision) {
		Destroy (gameObject);
	}
}
