﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;

	public float reloadTime = 1.0f;
	private float firedTime;
	private bool reloading = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0) {
			return;
		}

		if (Input.GetButtonDown ("Fire1") && reloading == false) {
			BulletMove bullet = Instantiate (bulletPrefab);

			bullet.transform.position = transform.position;

			Ray ray = 
				Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;
			reloading = true;
			firedTime = Time.time;
			Debug.Log ("Reloading");
		}
		if (reloading == true && Time.time >= (firedTime + reloadTime)) {
			reloading = false;
			Debug.Log ("Loaded");
		}
	}
}
